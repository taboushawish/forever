/**
 * Different types in Java:
 * class
 * enum
 * primitives
 * interfaces
 * 
 * A class is a combination of data and methods
 * 
 * An interface only defines methods that it's subclasses will have
 * By definition, everything in an interface is public

/**
 * common workflow in java:
 * 
 * Create an interface (let's us program in general)
 * Create an abstract class (let's us code shared functionality)
 * Create concrete classes
 * 
 * Abstract:
 * -Cannot be instantiated
 * -Concrete classes that inherit from the abstract class "must" implement
 * all the abstract methods
 * -Abstract subclasses 
 * @author unouser
 *
 */

public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}